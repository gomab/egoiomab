#eGoioMab

BLog website

## Development Environment

### Requirements

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Verify requirements :
 $ symfony check:requirements

## Start development environment 
 $ composer install
 $ docker-compose up -d
 $ symfony serve -d

### Run Tests
    $ php bin/phpunit --testdox
