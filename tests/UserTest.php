<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setName('name')
            ->setPassword('password')
            ->setAbout('about')
            ->setInstagram('instagram')
            ->setPhone('phone')
            ->setFacebook('facebook');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getName() === 'name');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAbout() === 'about');
        $this->assertTrue($user->getInstagram() === 'instagram');
        $this->assertTrue($user->getPhone() === 'phone');
        $this->assertTrue($user->getFacebook() === 'facebook');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setName('name')
            ->setPassword('password')
            ->setAbout('about')
            ->setInstagram('instagram')
            ->setPhone('phone')
            ->setFacebook('facebook');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getName() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAbout() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
        $this->assertFalse($user->getPhone() === 'false');
        $this->assertFalse($user->getFacebook() === 'false');
    }

    public function testIsEmpty(){
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getName());
//        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAbout());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getPhone());
        $this->assertEmpty($user->getFacebook());
    }


//    public function testSomething(): void
//    {
//        $this->assertTrue(true);
//    }
}
