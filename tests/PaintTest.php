<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Paint;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use DateTime;

class PaintTest extends TestCase
{
    public function testIsTrue()
    {
        $paint = new Paint();
        $datetime = new DateTime();
        $category = new Category();
        $user = new User();

        $paint->setName('name')
            ->setHeight(20.20)
            ->setWidth(20.20)
            ->setOnSale(true)
            ->setDateProduction($datetime)
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->setPrice(20.20)
            ->addCategory($category)
            ->setUser($user);

        $this->assertTrue($paint->getName() === 'name');
        $this->assertTrue($paint->getHeight() == 20.20);
        $this->assertTrue($paint->getWidth() == 20.20);
        $this->assertTrue($paint->getOnSale() === true);
        $this->assertTrue($paint->getDateProduction() === $datetime);
        $this->assertTrue($paint->getCreatedAt() === $datetime);
        $this->assertTrue($paint->getDescription() === 'description');
        $this->assertTrue($paint->getPortfolio() === true);
        $this->assertTrue($paint->getSlug() === 'slug');
        $this->assertTrue($paint->getFile() === 'file');
        $this->assertTrue($paint->getPrice() == 20.20);
        $this->assertContains($category, $paint->getCategory());
        $this->assertTrue($paint->getUser() === $user);
    }


    public function testIsFalse()
    {
        $paint = new Paint();
        $datetime = new DateTime();
        $category = new Category();
        $user = new User();

        $paint->setName('name')
            ->setHeight(20.20)
            ->setWidth(20.20)
            ->setOnSale(true)
            ->setDateProduction($datetime)
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->setPrice(20.20)
            ->addCategory($category)
            ->setUser($user);

        $this->assertFalse($paint->getName() === 'false');
        $this->assertFalse($paint->getHeight() == 22.20);
        $this->assertFalse($paint->getWidth() == 22.20);
        $this->assertFalse($paint->getOnSale() === false);
        $this->assertFalse($paint->getDateProduction() === new DateTime());
        $this->assertFalse($paint->getCreatedAt() === new DateTime());
        $this->assertFalse($paint->getDescription() === 'false');
        $this->assertFalse($paint->getPortfolio() === false);
        $this->assertFalse($paint->getSlug() === 'false');
        $this->assertFalse($paint->getFile() === 'false');
        $this->assertFalse($paint->getPrice() == 22.20);
        $this->assertContains($category, $paint->getCategory());
        $this->assertFalse($paint->getUser() === new User());
    }


    public function testIsEmpty()
    {
        $paint = new Paint();

        $this->assertEmpty($paint->getName());
        $this->assertEmpty($paint->getHeight());
        $this->assertEmpty($paint->getWidth());
        $this->assertEmpty($paint->getOnSale());
        $this->assertEmpty($paint->getDateProduction());
        $this->assertEmpty($paint->getCreatedAt());
        $this->assertEmpty($paint->getDescription());
        $this->assertEmpty($paint->getPortfolio());
        $this->assertEmpty($paint->getSlug());
        $this->assertEmpty($paint->getFile());
        $this->assertEmpty($paint->getPrice());
        $this->assertEmpty($paint->getCategory());
        $this->assertEmpty($paint->getUser());

    }
//    public function testSomething(): void
//    {
//        $this->assertTrue(true);
//    }
}
