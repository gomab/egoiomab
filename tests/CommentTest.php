<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\Paint;
use PHPUnit\Framework\TestCase;
use DateTime;

class CommentTest extends TestCase
{
    public function testIsTrue(){
        $comment = new Comment();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $paint = new Paint();

        $comment->setAuthor('author')
                ->setEmail('email@test.com')
                ->setCreatedAt($datetime)
                ->setContent('content')
                ->setBlogpost($blogpost)
                ->setPaint($paint);

        $this->assertTrue($comment->getAuthor() === 'author');
        $this->assertTrue($comment->getEmail() === 'email@test.com');
        $this->assertTrue($comment->getCreatedAt() === $datetime);
        $this->assertTrue($comment->getContent() === 'content');
        $this->assertTrue($comment->getBlogpost() === $blogpost);
        $this->assertTrue($comment->getPaint() === $paint);
    }

    public function testIsFalse(){
        $comment = new Comment();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $paint = new Paint();

        $comment->setAuthor('false')
            ->setEmail('false@test.com')
            ->setCreatedAt(new DateTime())
            ->setContent('false')
            ->setBlogpost(new BlogPost())
            ->setPaint(new Paint());

        $this->assertFalse($comment->getAuthor() === 'author');
        $this->assertFalse($comment->getEmail() === 'email@test.com');
        $this->assertFalse($comment->getCreatedAt() === $datetime);
        $this->assertFalse($comment->getContent() === 'content');
        $this->assertFalse($comment->getBlogpost() === $blogpost);
        $this->assertFalse($comment->getPaint() === $paint);
    }

    public function testIsEmpty(){
        $comment = new Comment();
        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getEmail());
        $this->assertEmpty($comment->getCreatedAt());
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getBlogpost());
        $this->assertEmpty($comment->getPaint());
    }



//    public function testSomething(): void
//    {
//        $this->assertTrue(true);
//    }
}
