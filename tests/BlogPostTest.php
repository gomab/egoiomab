<?php

namespace App\Tests;

use App\Entity\BlogPost;
use PHPUnit\Framework\TestCase;
use DateTime;

class BlogPostTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new BlogPost();
        $datetime = new DateTime();

        $blogpost->setTitle('title')
                 ->setCreatedAt($datetime)
                 ->setContent('content')
                 ->setSlug('slug')
                 ->setFile('file');

        $this->assertTrue($blogpost->getTitle() === 'title');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
        $this->assertTrue($blogpost->getContent() === 'content');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getFile() === 'file');
    }

    public function testIsFalse()
    {
        $blogpost = new BlogPost();
        $datetime = new DateTime();

        $blogpost->setTitle('title')
            ->setCreatedAt($datetime)
            ->setContent('content')
            ->setSlug('slug')
            ->setFile('file');

        $this->assertFalse($blogpost->getTitle() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogpost->getContent() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
        $this->assertFalse($blogpost->getFile() === 'false');
    }

    public function testIsEmpty()
    {
        $blogpost = new BlogPost();

        $this->assertEmpty($blogpost->getTitle());
        $this->assertEmpty($blogpost->getCreatedAt());
        $this->assertEmpty($blogpost->getContent());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getFile());
    }

//    public function testSomething(): void
//    {
//        $this->assertTrue(true);
//    }
}
